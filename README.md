<!-- /**
 * @author N3UR0515
 * @email alfie@neurosis.uk
 * @create date 14-08-2022 14:30:36
 * @modify date 14-08-2022 14:30:36
 * @desc NeuroVision Readme
 */ -->

# Media Server

Automated media server setup based on Docker using a reverse proxy for secure connections.

**Note:** A better way of utilising this Docker media server would be to build it on top of [OpenMediaVault](https://docs.neurosis.uk/v/openmediavault/)

[![pipeline status](https://gitlab.com/N3UR0515/NeuroVision/badges/main/pipeline.svg)](https://gitlab.com/N3UR0515/NeuroVision/commits/main)

----

## Containers

- [rTorrent](#rtorrent)

- [Jackett](#jackett)

- [Sonarr](#sonarr)

- [Radarr](#radarr)

- [Ombi](#ombi)

- [Tautulli](#tautulli)

- [Plex](#plex)

----

## Requirements

You need to have already purchased a VPN service through a provider that offers OpenVPN connections, such as [PIA](https://www.privateinternetaccess.com/), [NordVPN](https://nordvpn.com/), [CyberGhost](https://www.cyberghostvpn.com/) etc.

I currently use [CyberGhost](https://www.cyberghostvpn.com/).

**Note:** You can use WireGuard instead of OpenVPN, however I will not be covering that in this guide just yet. See here for more information: <https://hub.docker.com/r/binhex/arch-rtorrentvpn>

You will also need to install `Git`, if not already installed:

`sudo apt update && sudo apt install git -y`

Now clone the media server repository into your `home directory`:

`cd ~/ && git clone https://gitlab.com/N3UR0515/NeuroVision.git`

----

## Variables

All variables (Passwords, Usernames, API Keys etc.) will be defined in the `.env` file.

Rename the included `.env-example` to `.env` and update the info and passwords as needed.

**Warning:** Be sure to update the `.env` file **before** deploying, so that it is appropriate to your use case. For example, you will need to update the `PHP_TZ` and `PLEX_CLAIM` parameters (explained below), to that of your **timezone** and your *unique* **Plex claim token**.

- ### `PHP_TZ`

  - Set the `timezone` (*for example `Europe/London`*). The complete list can be [found here](https://en.wikipedia.org/wiki/List_of_tz_database_time_zones).

- ### `PLEX_CLAIM`

  - The claim token used to connect the media server to your Plex account. If not provided, the server will not be automatically logged in. If server is already logged in, this parameter is ignored. You can use [this link](https://www.plex.tv/claim) to obtain a claim token for your Plex server.

----

## Networking

**Warning:** If you **do not** use the same `network name` as defined here, be sure to update the `DOCKER_NETWORK` variable in your `.env` file with the correct name.

Create `mediaNet` network for Media containers

    docker network create mediaNet

----

## rTorrent

Arch Linux running rTorrent with ruTorrent Web UI and OpenVPN by [binhex](https://github.com/binhex) on [DockerHub](https://hub.docker.com/r/binhex/arch-rtorrentvpn).

**Info:** *Used to download the torrents through a VPN connection*

Once you have purchased your VPN service, download the `OpenVPN Connection` files from your VPN provider. Extract the files if needed and copy them to the `rtorrent\openvpn` folder, as shown in the image below (*You may need to deploy the containers once first, to create the folder structures*).

![rTorrent Files](https://gitlab.com/N3UR0515/docker-wiki/-/raw/e315599fb7717ed6902da897cb231d97ee4fc990/images/vpnfiles.png)

**Ports:**

    9080  (Web GUI HTTP port)
    9443  (Web GUI SSL port)
    8118  (Privoxy port)
    5000  (RPC2 port)

----

## Jackett

Jackett container by [linuxserver.io](https://www.linuxserver.io/) on [DockerHub](https://hub.docker.com/r/linuxserver/jackett). Connects to the `Privoxy` service running on the `rTorrent` container (port `8118`).

**Info:** *Used to search for the `Torrents` to be downloaded via the `VPN`*

**Ports:**

    9117

----

## Sonarr

Sonarr container by [linuxserver.io](https://www.linuxserver.io/) on [DockerHub](https://hub.docker.com/r/linuxserver/sonarr)

**Info:** *Manages the TV shows we want to download and monitor*

**Ports:**

    8989

----

## Radarr

Radarr container by [linuxserver.io](https://www.linuxserver.io/) on [DockerHub](https://hub.docker.com/r/linuxserver/radarr)

**Info:** *Manages the movies we want to download and monitor*

**Ports:**

    7878

----

## Ombi

Ombi container by [linuxserver.io](https://www.linuxserver.io/) on [DockerHub](https://hub.docker.com/r/linuxserver/ombi)

**Info:** *Adds an easy to use interface (with optional mobile apps available), to search for shows and movies for the `Sonarr` and `Radarr` containers to monitor*

**Ports:**

    3579

----

## Tautulli

Tautulli container by [linuxserver.io](https://www.linuxserver.io/) on [DockerHub](https://hub.docker.com/r/linuxserver/tautulli)

**Info:** *Used to manage the Plex server below*

**Ports:**

    8181

----

## Plex

Plex container by [linuxserver.io](https://www.linuxserver.io/) on [DockerHub](https://hub.docker.com/r/linuxserver/plex)

**Info:** *Used to serve the media files*

**Ports:**

    32400

----
